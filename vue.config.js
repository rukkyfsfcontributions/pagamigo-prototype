module.exports = {
  outputDir: "./build",
  css: {
    extract: false
  },
  chainWebpack: config => {
    config.optimization.delete("splitChunks");
  },

  configureWebpack: {
    output: {
      filename: "bundle.js"
    },
    optimization: {
      minimize: true
    }
  }
};
