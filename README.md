# Pagamigo
Pagamigo is a project that allows users make Paypal payments without using proprietary Javascript in the browser.

This package is licensed under AGPL-3.0-only

This browser package ought to be used along with the Pagamigo CLI which is available at: [https://notabug.org/alyssa/pagamigo](https://notabug.org/alyssa/pagamigo)

## Project setup
To use the pagamigo package, you can either build the Javascript bundle with `yarn build` or use the `bundle.js` file provided in the `output` folder of this repository. The `bundle.js` file in the `output` folder is the result of building the project.

If you choose to build it yourself, you can configure the build settings in the `vue.config.js` file. Also, if you're building it yourself, you will need to recreate the `public/css` folder. Specifically, `normalize.css` and 'skeleton.css' should be in the `public/css` folder. You can get those from the `Skeleton` repository here: [https://github.com/dhg/Skeleton/](https://github.com/dhg/Skeleton/)

Regardless of how you choose to go about it, after setting up the project, you should have a `bundle.js` file.

## How it Works
The Pagamigo browser bundle works by creating a PayPal Order and displaying the `approve` Url for that order to the user.

The user can then use that url with the `Pagamigo CLI` to approve the transaction.

After approving the transaction the user can click the confirmation button to initiate a payment capture.

## Project Installation
To use Pagamigo, include the `bundle.js` file in any html document you want to display the Pagamigo button in and create a `div` element with `id=pagamigo` like so:

```
// Omit the data-sandbox attribute if you want to run Pagamigo against Paypal's live API
<div id="pagamigo"
     data-btn-text="Pay with Pagamigo"
     data-amount="50" 
     data-currency-code="USD"
     data-paypal-access-token="replace-with-paypal-oauth-access-token"
     data-sandbox
></div>

<script src="bundle.js"></script>

<script>
  let pagamigoEl = document.getElementById("pagamigo");
  pagamigoEl.addEventListener("payment-success", function (paymentDetails) {
    console.log(paymentDetails.detail);
  });

  pagamigoEl.addEventListener("payment-failure", function (errorDetails) {
    console.log(errorDetails);
  });
</script>
```

The access token should be generated on the server using your app's Paypal credentials. 

Pagamigo emits 2 events that you can respond to as shown in the snippet above.

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
